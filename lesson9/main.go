package main

import (
	"fmt"
	"config_getter/config_getter"
	"os"
)

func main() {
	os.Setenv("WEB_PORT", "8080")
	conf, err := config_getter.GetConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
	fmt.Printf("CONF: %#v\n", conf)
}
