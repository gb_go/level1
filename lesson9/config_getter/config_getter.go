package config_getter

import (
	"flag"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
	"log"
	"net/url"
	"os"
	"strings"
)

type ServerConfig struct {
	WebPort     int    `envconfig:"WEB_PORT" yaml:"port" default:"80" required:"true"`
	DBURL       string `envconfig:"DB_URL" yaml:"db_url" default:"postgres://db-user:db-password@petstore-db:5432/petstore?sslmode=disable" required:"true"`
	JaegerURL   string `envconfig:"JAEGER_URL" yaml:"jaeger_url" default:"http://jaeger:16686" required:"true"`
	SentryURL   string `envconfig:"SENTRY_URL" yaml:"sentry_url" default:"http://sentry:9000" required:"true"`
	KafkaBroker string `envconfig:"KAFKA_BROKER" yaml:"kafka_broker" default:"kafka:9092" required:"true"`
	APPID       string `envconfig:"APP_ID" yaml:"some_app_id" default:"testid" required:"true"`
	APPKey      string `envconfig:"APP_KEY" yaml:"some_app_key" default:"testkey" required:"true"`
}

func validateURL(conf ServerConfig) (err error) {
	if _, err = url.ParseRequestURI(conf.DBURL); err != nil {
		err = fmt.Errorf("Не верно введен DBURL: %v", err)
		return
	}
	if _, err = url.ParseRequestURI(conf.JaegerURL); err != nil {
		err = fmt.Errorf("Не верно введен JaegerURL: %v", err)
		return
	}
	if _, err = url.ParseRequestURI(conf.SentryURL); err != nil {
		err = fmt.Errorf("Не верно введен SentryURL: %v", err)
		return
	}
	if _, err = url.ParseRequestURI(conf.KafkaBroker); err != nil {
		err = fmt.Errorf("Не верно введен KafkaBroker: %v", err)
		return
	}
	return
}

func getConfigYaml(conf *ServerConfig, filename string) {
	if _, err := os.Stat(filename); err != nil {
		log.Fatalf("Файл не существует: %v", err)
	}
	file, err := os.Open(filename)
	if err != nil {
		log.Fatalf("Не могу открыть файл: %v", err)
	}
	defer func() {
		err2 := file.Close()
		if err2 != nil {
			log.Printf("Не могу закрыть файл: %v", err2)
		}
	}()
	err = yaml.NewDecoder(file).Decode(&conf)
	if err != nil {
		log.Fatalf("Не могу декодировать yaml: %v", err)
	}
}

func GetConfig() (conf ServerConfig, error error) {
	var (
		WebPort     = flag.Int("webport", 81, "Порт web-сервера")
		DBURL       = flag.String("dburl", "postgres://db-user:db-password@petstore-db:5431/petstore?sslmode=disable", "Путь подключения к БД")
		JaegerURL   = flag.String("jaegerurl", "http://jaeger:16685", "Ссылка на Jaeger")
		SentryURL   = flag.String("sentryurl", "http://sentry:9001", "Ссылка на Sentry")
		KafkaBroker = flag.String("kafkabroker", "kafka:9090", "Ссылка на Kafka")
		APPID       = flag.String("appid", "testid2", "ID для получения доступа")
		APPKey      = flag.String("appkey", "testkey2", "Ключ для получения доступа")
	)
	err := envconfig.Process("", &conf)
	if err != nil {
		log.Printf("Не могу декодировать Env: %v", err)
	}
	flag.Parse()
	if flag.NArg() == 1 {
		filename := strings.TrimSpace(flag.Arg(0))
		getConfigYaml(&conf, filename)
	}
	flag.Visit(func(flag *flag.Flag) {
		switch flag.Name {
		case "webport":
			conf.WebPort = *WebPort
		case "dburl":
			conf.DBURL = *DBURL
		case "jaegerurl":
			conf.JaegerURL = *JaegerURL
		case "sentryurl":
			conf.SentryURL = *SentryURL
		case "kafkabroker":
			conf.KafkaBroker = *KafkaBroker
		case "appid":
			conf.APPID = *APPID
		case "appkey":
			conf.APPKey = *APPKey
		}
	})
	error = validateURL(conf)
	return
}
