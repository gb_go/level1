package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main()  {
	inputNums := enterNumbers()
	if inputNums != nil {
		insertionSort(inputNums)
		fmt.Println(inputNums)
	} else {
		fmt.Println("Массив для сортировки пуст")
	}
}

func enterNumbers() (a []int64) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Вводити целые числа по одному в строку, ввод любых значений кроме целого числа завершает ввод:")
	for scanner.Scan() {
		num, err := strconv.ParseInt(scanner.Text(), 10, 64)
		if err != nil {
			break
		}
		a = append(a, num)
	}
	return
}

func insertionSort(inputNums []int64) {
	for j := 1; j < len(inputNums); j++ {
		key := (inputNums)[j]
		i := j-1
		for i >= 0 && (inputNums)[i] > key {
			(inputNums)[i+1] = (inputNums)[i]
			i--
		}
		(inputNums)[i+1] = key
	}
}