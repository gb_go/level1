package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func main() {
	choice := choiceAction()
	for {
		switch choice {
		case 1:
			calculator()
		case 2:
			findPrimeNumberEratosfen()
		case 0:
			os.Exit(0)
		}
		choice = choiceAction()
	}
}

func choiceAction() int8 {
	var choice int8
	for {
		fmt.Println("Выберите операцию:")
		fmt.Println("1. Калькулятор")
		fmt.Println("2. Поиск простых чисел в диапазоне от 0 до N. ")
		fmt.Println("0. Выход")
		if _, err := fmt.Scanln(&choice); err != nil {
			fmt.Println("Введите цифру от 1 до 2 или 0")
		} else if choice >= 0 && choice <= 2 {
			return choice
		} else {
			fmt.Println("Введите цифру от 1 до 2 или 0")
		}
	}
}

func discardBuffer(r *bufio.Reader) {
	r.Discard(r.Buffered())
}

func enterOperands() (a, b float64, err error) {
	stdin := bufio.NewReader(os.Stdin)
	fmt.Println("Введите операнд")
	if _, ok := fmt.Fscanln(stdin, &a, &b); ok != nil {
		discardBuffer(stdin)
		err = fmt.Errorf("Не верный ввод\n")
	}
	return a, b, err
}

func enterOperand() (a float64, err error) {
	stdin := bufio.NewReader(os.Stdin)
	fmt.Println("Введите операнд")
	if _, ok := fmt.Fscanln(stdin, &a); ok != nil {
		discardBuffer(stdin)
		err = fmt.Errorf("Не верный ввод\n")
	}
	return a, err
}

func printResult(result float64) {
	fmt.Printf("%f\n\n", result)
}

func calculator() {
	var choice int8
	var a, b, result float64
	var err error
	for {
		fmt.Println("Выберите операцию:")
		fmt.Println("1. Сложение +")
		fmt.Println("2. Вычитание -")
		fmt.Println("3. Умножение *")
		fmt.Println("4. Деление /")
		fmt.Println("5. Квадрат")
		fmt.Println("6. Корень квадратный")
		fmt.Println("7. Логарифм log")
		fmt.Println("0. Выход")
		if _, ok := fmt.Scanln(&choice); ok != nil {
			fmt.Println("Введите цифру от 1 до 7 или 0")
			continue
		} else if choice == 0 {
			break
		} else if !(choice >= 0 && choice <= 7) {
			fmt.Println("Введите цифру от 1 до 7 или 0")
			continue
		}
		for {
			switch choice {
			case 1:
				if a, b, err = enterOperands(); err != nil {
					break
				}
				result = a + b
			case 2:
				if a, b, err = enterOperands(); err != nil {
					break
				}
				result = a - b
			case 3:
				if a, b, err = enterOperands(); err != nil {
					break
				}
				result = a * b
			case 4:
				if a, b, err = enterOperands(); err != nil {
					break
				}
				if b == 0 {
					err = fmt.Errorf("Делить на 0 нельзя!\n")
					break
				}
				result = a / b
			case 5:
				if a, err = enterOperand(); err != nil {
					break
				}
				result = a * a
			case 6:
				if a, err = enterOperand(); err != nil {
					break
				}
				result = math.Sqrt(a)
			case 7:
				if a, err = enterOperand(); err != nil {
					break
				}
				result = math.Log(a)
			}
			if err != nil {
				fmt.Println(err)
			} else {
				printResult(result)
			}
			break
		}
	}
}

func findPrimeNumberEratosfen() {
	var n int
	fmt.Println("Введите правую границу")
	if _, err := fmt.Scanln(&n); err != nil {
		fmt.Println("Не верный ввод\n")
		return
	}
	pr := []int{}
	for i := 2; i <= n; i++ {
		isPrime := true
		for j := 2; j < i; j++ {
			if i%j == 0 {
				isPrime = false
				break
			}
		}
		if isPrime == true {
			pr = append(pr, i)
		}
		isPrime = true
	}
	fmt.Println(pr)
}