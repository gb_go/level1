package main

import (
	"fmt"
	"math"
	"os"
)

func main()  {
	choice := choiceAction()
	for {
		switch choice {
		case 1:
			rectangleSquare()

		case 2:
			calculateDiameterAndCircleLength()

		case 3:
			digits()

		case 4:
			calculator()

		case 5:
			findPrimeNumber()

		case 0:
			os.Exit(1)
		}
		choice = choiceAction()
	}
}

func choiceAction() int8 {
	var choice int8
	for {
		fmt.Println("Выберите операцию:")
		fmt.Println("1. Вычисление площади прямоугольника")
		fmt.Println("2. Вычисление диаметра и длины окружности")
		fmt.Println("3. Разбиение числа на сотни, десятки и единицы")
		fmt.Println("4. Калькулятор")
		fmt.Println("5. Поиск простых чисел в диапазоне от 0 до N")
		fmt.Println("0. Выход")
		_, err := fmt.Scanln(&choice)
		if err != nil {
			fmt.Println("Введите цифру от 1 до 5 или 0")
		} else if choice >= 0 && choice <= 5 {
			return choice
		} else {
			fmt.Println("Введите цифру от 1 до 5 или 0")
		}
	}
}

func rectangleSquare() {
	var a, b int
	fmt.Println("Введите длины сторон через пробел")
	_, err := fmt.Scanln(&a, &b)
	if err != nil {
		fmt.Println("Не верный ввод\n")
	} else {
		s := a * b
		fmt.Printf("Площадь составляет: %d\n\n", s)
	}
}

func calculateDiameterAndCircleLength() {
	var s float64
	fmt.Println("Введите площадь круга")
	_, err := fmt.Scanln(&s)
	if err != nil {
		fmt.Println("Не верный ввод\n")
	} else {
		d := 2 * (math.Sqrt(s / math.Pi))
		l := math.Pi * d
		fmt.Printf("Диаметр составляет: %.2f\nДлина окружности составляет: %.2f\n\n", d, l)
	}
}

func digits() {
	var n int16
	fmt.Println("Введите трехзначное число")
	_, err := fmt.Scanln(&n)
	if err != nil {
		fmt.Println("Не верный ввод\n")
	} else {
		if n > 99 && n < 1000 {
			var units, tens, hundreds int16
			units = n % 10
			n = n / 10
			tens = n % 10
			n = n / 10
			hundreds = n
			fmt.Printf("Сотен: %d\nДесятков: %d\nЕдиниц: %d\n\n", hundreds, tens, units)
		} else {
			fmt.Println("Введено не трехзначное число\n")
		}
	}
}

func calculator()  {
	var choice int8
	var a, b float64
	for {
		fmt.Println("Выберите операцию:")
		fmt.Println("1. Сложение +")
		fmt.Println("2. Вычитание -")
		fmt.Println("3. Умножение *")
		fmt.Println("4. Деление /")
		fmt.Println("0. Выход")
		_, err := fmt.Scanln(&choice)
		if err != nil {
			fmt.Println("Введите цифру от 1 до 4 или 0")
		} else if choice == 0 {
			break
		} else if !(choice >= 0 && choice <= 4) {
			fmt.Println("Введите цифру от 1 до 4 или 0")
		} else {
			for {
				fmt.Println("Введите операнды через пробел")
				_, err = fmt.Scanln(&a, &b)
				if err != nil {
					fmt.Println("Не верный ввод\n")
				} else {
					switch choice {
					case 1:
						fmt.Printf("%f\n\n", a+b)
					case 2:
						fmt.Printf("%f\n\n", a-b)
					case 3:
						fmt.Printf("%f\n\n", a*b)
					case 4:
						if b == 0 {
							fmt.Println("Делить на 0 нельзя!\n")
						} else {
							fmt.Printf("%f\n\n", a/b)
						}
					}
					break
				}
			}
		}
	}
}

func findPrimeNumber () {
	var n int
	fmt.Println("Введите правую границу")
	_, err := fmt.Scanln(&n)
	if err != nil {
		fmt.Println("Не верный ввод\n")
	} else {
		brk := false
		lst := []int{2}
		for i := 3; i <= n; i = i + 2 {
			if i > 10 && i%10 == 5 {
				continue
			}
			for _, j := range lst {
				if j*j-1 > i {
					lst = append(lst, i)
					brk = true
					break
				}
				if i%j == 0 {
					brk = true
					break
				}
			}
			if !brk {
				lst = append(lst, i)
			}
		}
		fmt.Println(lst)
	}
}