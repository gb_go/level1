package config_getter

import (
	"flag"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"net/url"
)

type ServerConfig struct {
	WebPort     int    `envconfig:"WEB_PORT" default:"80" required:"true"`
	DBURL       string `envconfig:"DB_URL" default:"postgres://db-user:db-password@petstore-db:5432/petstore?sslmode=disable" required:"true"`
	JaegerURL   string `envconfig:"JAEGER_URL" default:"http://jaeger:16686" required:"true"`
	SentryURL   string `envconfig:"SENTRY_URL" default:"http://sentry:9000" required:"true"`
	KafkaBroker string `envconfig:"KAFKA_BROKER" default:"kafka:9092" required:"true"`
	APPID       string `envconfig:"APP_ID" default:"testid" required:"true"`
	APPKey      string `envconfig:"APP_KEY" default:"testkey" required:"true"`
}

func validateURL(conf ServerConfig) (err error) {
	if _, err = url.ParseRequestURI(conf.DBURL); err != nil {
		err = fmt.Errorf("Не верно введен DBURL: %v", err)
		return
	}
	if _, err = url.ParseRequestURI(conf.JaegerURL); err != nil {
		err = fmt.Errorf("Не верно введен JaegerURL: %v", err)
		return
	}
	if _, err = url.ParseRequestURI(conf.SentryURL); err != nil {
		err = fmt.Errorf("Не верно введен SentryURL: %v", err)
		return
	}
	if _, err = url.ParseRequestURI(conf.KafkaBroker); err != nil {
		err = fmt.Errorf("Не верно введен KafkaBroker: %v", err)
		return
	}
	return
}

func GetConfig() (conf ServerConfig, err error) {
	var (
		WebPort     = flag.Int("webport", 81, "Порт web-сервера")
		DBURL       = flag.String("dburl", "postgres://db-user:db-password@petstore-db:5431/petstore?sslmode=disable", "Путь подключения к БД")
		JaegerURL   = flag.String("jaegerurl", "http://jaeger:16685", "Ссылка на Jaeger")
		SentryURL   = flag.String("sentryurl", "http://sentry:9001", "Ссылка на Sentry")
		KafkaBroker = flag.String("kafkabroker", "kafka:9090", "Ссылка на Kafka")
		APPID       = flag.String("appid", "testid2", "ID для получения доступа")
		APPKey      = flag.String("appkey", "testkey2", "Ключ для получения доступа")
	)
	envconfig.Process("", &conf)
	flag.Parse()
	flag.Visit(func(flag *flag.Flag) {
		switch flag.Name {
		case "webport":
			conf.WebPort = *WebPort
		case "dburl":
			conf.DBURL = *DBURL
		case "jaegerurl":
			conf.JaegerURL = *JaegerURL
		case "sentryurl":
			conf.SentryURL = *SentryURL
		case "kafkabroker":
			conf.KafkaBroker = *KafkaBroker
		case "appid":
			conf.APPID = *APPID
		case "appkey":
			conf.APPKey = *APPKey
		}
	})
	err = validateURL(conf)
	return
}
