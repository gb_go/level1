package main

import (
	"fmt"
	"os"
	"time"
)

var fibCache = make(map[uint]uint64)

func fibonachi(n uint, m bool) (num uint64) {
	if !m {
		if n == 0 || n == 1 {
			return uint64(n)
		}
		return fibonachi(n-1, m) + fibonachi(n-2, m)
	}

	if n == 0 || n == 1 {
		num = uint64(n)
		fibCache[n] = num
		return
	}
	if num, ok := fibCache[n]; ok {
		return num
	}
	num = fibonachi(n-1, m) + fibonachi(n-2, m)
	fibCache[n] = num
	return
}

func getNumber() (n uint, err error) {
	fmt.Println("Введите номер числа Фибоначчи для расчета (для выхода введите 0)")
	if _, ok := fmt.Scanln(&n); ok != nil {
		err = fmt.Errorf("не верный ввод")
	}
	return
}

func main() {
	for {
		n, err := getNumber()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if n == 0 {
			break
		}
		fmt.Println("-----without cache-------------")
		start1 := time.Now()
		fmt.Println(fibonachi(n, false))
		runTime1 := time.Since(start1)
		fmt.Printf("Run time is %s \n", runTime1)
		fmt.Println("-----with cache----------------")
		start2 := time.Now()
		fmt.Println(fibonachi(n, true))
		runTime2 := time.Since(start2)
		fmt.Printf("Run time is %s \n", runTime2)
		fmt.Println("-------------------------------")
		fmt.Println()
	}
}
